package com.company;


import java.util.*;

public class BinarySearchTreeImpl implements BinarySearchTree {

    private BinaryTreeNode root;

    public BinarySearchTreeImpl() {
        root = null;
    }

    public BinarySearchTreeImpl(BinaryTreeNode root) {
        this.root = root;
    }

    @Override
    public BinaryTreeNode getRoot() {
        return root;
    }

    @Override
    public void insert(int value) {
        BinaryTreeNode tempNode, checker;
        tempNode = new BinaryTreeNode(value);
        if(root == null){
            root = tempNode;
        } else {
            checker = root;
            while (checker != null){
                if(value > checker.getValue()){
                    if(checker.getRightChild() == null){
                        checker.setRightChild(tempNode);
                        break;
                    } else {
                        checker = checker.getRightChild();
                    }
                } else {
                    if(checker.getLeftChild() == null){
                        checker.setLeftChild(tempNode);
                        break;
                    } else {
                        checker = checker.getLeftChild();
                    }
                }
            }
        }
    }


    @Override
    public BinaryTreeNode search(int value) {
       if(root == null){
           return null;
       }
       return dfs(value, root);
    }

    private BinaryTreeNode dfs(int value, BinaryTreeNode root){
        if(root == null || root.getValue() == value){
            return root;
        }
        if(root.getValue() > value){
            return dfs(value, root.getLeftChild());
        }
        return dfs(value, root.getRightChild());
    }

    @Override
    public List<Integer> inOrder() {
        List<Integer> list = new ArrayList<>();
        if(root == null){
            return list;
        }
        Stack<BinaryTreeNode> stack = new Stack<>();
        BinaryTreeNode node = root;

        while(node != null || stack.size() > 0){
            while (node != null){
                stack.push(node);
                node = node.getLeftChild();
            }
            node = stack.pop();

            list.add(node.getValue());

            node = node.getRightChild();
        }
        return list;
    }

    public List<Integer> inOrderRec(){
        List<Integer> list = new ArrayList<>();
        ior(root, list);
        return list;
    }

    private void ior(BinaryTreeNode root, List<Integer> list){
        if(root == null){
            return;
        }
        ior(root.getLeftChild(), list);
        list.add(root.getValue());
        ior(root.getRightChild(), list);
    }

    @Override
    public List<Integer> postOrder() {
        List<Integer> list = new ArrayList<>();
        if(root == null){
            return list;
        }
        Stack<BinaryTreeNode> stack = new Stack<>();
        stack.push(root);
        BinaryTreeNode previous = null;

        while(!stack.isEmpty()){
            BinaryTreeNode current = stack.peek();
            if(previous == null || previous.getLeftChild() == current || previous.getRightChild() == current){
                if(current.getLeftChild() != null){
                    stack.push(current.getLeftChild());
                }else if(current.getRightChild() != null){
                    stack.push(current.getRightChild());
                } else {
                    stack.pop();
                    list.add(current.getValue());
                }
            } else if(current.getLeftChild() == previous){
                if(current.getRightChild() != null){
                    stack.push(current.getRightChild());
                }else {
                    stack.pop();
                    list.add(current.getValue());
                }
            }else if(current.getRightChild() == previous){
                stack.pop();
                list.add(current.getValue());
            }
            previous = current;
        }
        return list;
    }

    public List<Integer> postOrderRec(){
        List<Integer> list = new ArrayList<>();
        por(root, list);
        return list;
    }

    private void por(BinaryTreeNode root, List<Integer> list){
        if(root == null){
            return;
        }
        por(root.getLeftChild(), list);
        por(root.getRightChild(), list);
        list.add(root.getValue());
    }

    @Override
    public List<Integer> preOrder() {
        List<Integer> list = new ArrayList<>();
        if(root == null){
            return list;
        }
        Stack<BinaryTreeNode> stack = new Stack<>();
        stack.push(root);

        while(!stack.isEmpty()){
            BinaryTreeNode node = stack.peek();
            list.add(node.getValue());
            stack.pop();

            if(node.getRightChild() != null){
                stack.push(node.getRightChild());
            }
            if(node.getLeftChild() != null){
                stack.push(node.getLeftChild());
            }
        }

        return list;
    }

    public List<Integer> preOrderRec(){
        List<Integer> list = new ArrayList<>();
        preor(root, list);
        return list;
    }

    private void preor(BinaryTreeNode root, List<Integer> list){
        if(root == null){
            return;
        }
        list.add(root.getValue());
        preor(root.getLeftChild(), list);
        preor(root.getRightChild(), list);
    }

    @Override
    public List<Integer> bfs() {
        List<Integer> list = new ArrayList<>();
        if(root == null){
            return list;
        }

        Queue<BinaryTreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()){
            BinaryTreeNode current = queue.poll();

            list.add(current.getValue());

            if(current.getLeftChild() != null) queue.offer(current.getLeftChild());
            if(current.getRightChild() != null) queue.offer(current.getRightChild());
        }
        return list;
    }

    public List<Integer> bfsRec(){
        List<Integer> list = new ArrayList<>();
        bfsrec(root, list);
        return list;
    }

    private void bfsrec(BinaryTreeNode root, List<Integer> list){
        if(root == null){
            return;
        }

        Queue<BinaryTreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            BinaryTreeNode current = queue.poll();
            list.add(current.getValue());
            if(current.getLeftChild() != null) queue.offer(current.getLeftChild());
            if(current.getRightChild() != null) queue.offer(current.getRightChild());
        }
    }

    @Override
    public int height() {
//        if(root == null){
//            return -1;
//        }
//        if(root.getRightChild() == null && root.getLeftChild()== null){
//            return 0;
//        }
//        int leftHeight = 0, rightHeight = 0;
//        if(root.getLeftChild() != null){
//            root = root.getLeftChild();
//            leftHeight = 1 + height();
//        }
//        if(root.getRightChild() != null){
//            root = root.getRightChild();
//            rightHeight = 1+ height();
//        }
//        return 1 + Math.max(leftHeight, rightHeight);
        return getHeigth(root);
    }

    private int getHeigth(BinaryTreeNode root){
        if(root == null){
            return -1;
        }
        return 1+ Math.max(getHeigth(root.getLeftChild()), getHeigth(root.getRightChild()));
    }
}