package com.telerikacademy;


public class QueueImpl<T> implements Queue<T> {
    private static final String QUEUE_IS_EMPTY = "There are no elements in the queue.";

    private Node head;
    private Node tail;
    private int size;


    @Override
    public void offer(T elem) {
        Node node = new Node(elem);
        if(tail == null){
            tail = node;
            head = node;
            size++;
            return;
        }
        tail.next = node;
        tail = node;
        size++;
    }

    @Override
    public T poll() {
        if(head == null){
            throw new IllegalArgumentException(QUEUE_IS_EMPTY);
        }
        Node node = head;
        head = head.next;
        if(head == null){
            tail = null;
        }
        size--;
        return node.data;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T peek() {
        if(head == null){
            throw new IllegalArgumentException(QUEUE_IS_EMPTY);
        }
        return head.data;
    }

    @Override
    public int size() {
        return size;
    }

    private class Node{
        private T data;
        private Node next;

        Node(T data){
            this.data = data;
        }
    }
}
