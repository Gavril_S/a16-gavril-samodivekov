package com.telerikacademy;


import java.util.Arrays;

public class StackImpl<T> implements Stack<T> {
    private static final int CAPACITY = 5;
    private static final int EMPTY_STACK_VALUE = -1;
    private static final String EMPTY_STACK_MESSAGE = "The stack is empty!";

    private T[] arr;
    private int size;
    private int top;

    @SuppressWarnings("unchecked")
    public StackImpl(){
        arr = ((T[]) new Object[CAPACITY]);
        size = 0;
        top = EMPTY_STACK_VALUE;
    }

    @Override
    public void push(T elem) {
        if(size == arr.length){
            resize();
        }
        top++;
        arr[top] = elem;
        size++;
    }

    @Override
    public T pop() {
        if(size == 0){
            throw new IllegalArgumentException(EMPTY_STACK_MESSAGE);
        }
        T value = arr[top];
        arr[top] = null;
        top--;
        size--;
        return value;
    }

    @Override
    public T peek() {
        if(size == 0){
            throw new IllegalArgumentException(EMPTY_STACK_MESSAGE);
        }
        return arr[top];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return top == EMPTY_STACK_VALUE;
    }

    private void resize(){
        arr = Arrays.copyOf(arr, arr.length*2);
    }
}
