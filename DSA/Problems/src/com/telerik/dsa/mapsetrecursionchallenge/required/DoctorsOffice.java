package com.telerik.dsa.mapsetrecursionchallenge.required;

import java.util.*;

public class DoctorsOffice {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LinkedList<String> inputList = new LinkedList<>();
        boolean flag = false;
        while(!flag){
            String input = scanner.nextLine();
            inputList.add(input);
            if(input.equalsIgnoreCase("end")){
                flag = true;
            }
        }

        LinkedList<String> patients = new LinkedList<>();
        Map<String, Integer> peopleCount = new HashMap<>();
        List<String> list = new ArrayList<>();

        queueHandle(patients, peopleCount, list, inputList);

        for (String elem : list) {
            System.out.println(elem);
        }
    }

    private static List<String> queueHandle(LinkedList<String> patients, Map<String, Integer> peopleCount,
                                            List<String> list, LinkedList<String> inputList){
        String[] input = inputList.getFirst().split(" ");

        if(input[0].equalsIgnoreCase("End")){
            return list;
        }

        if(input[0].equalsIgnoreCase("Append")){
            patients.add(input[1]);
            addPersonToMap(peopleCount, input[1]);
            list.add("OK");
        }

        if(input[0].equalsIgnoreCase("Insert")){
            if(Integer.parseInt(input[1]) < 0 || Integer.parseInt(input[1]) > patients.size()){
                list.add("Error");
            } else {
                patients.add(Integer.parseInt(input[1]), input[2]);
                addPersonToMap(peopleCount, input[2]);
                list.add("OK");
            }
        }

        if(input[0].equalsIgnoreCase("Find")){
            list.add(peopleCount.getOrDefault(input[1], 0).toString());
        }

        if(input[0].equalsIgnoreCase("Examine")){
            if(Integer.parseInt(input[1]) > patients.size()){
                list.add("Error");
            } else {
                StringBuilder strBuilder = new StringBuilder();
                for(int i = 0; i < Integer.parseInt(input[1]); i++){
                    if(peopleCount.get(patients.getFirst()) > 1){
                        peopleCount.put(patients.getFirst(), peopleCount.get(patients.getFirst()) - 1);
                    } else {
                        peopleCount.remove(patients.getFirst());
                    }
                    strBuilder.append(patients.getFirst());
                    strBuilder.append(" ");
                    patients.removeFirst();
                }
                strBuilder.setLength(strBuilder.length() - 1);
                list.add(strBuilder.toString());
            }
        }
        inputList.removeFirst();
        return queueHandle(patients, peopleCount, list, inputList);
    }

    private static void addPersonToMap(Map<String, Integer> peopleCount, String key) {
        if(peopleCount.containsKey(key)){
            peopleCount.put(key, peopleCount.get(key) + 1);
        } else {
            peopleCount.put(key, 1);
        }
    }
}