package com.telerik.dsa.mapsetrecursionchallenge.required;

import java.util.Scanner;

public class ScroogeMcDuck {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] dimensions = scanner.nextLine().split(" ");
        int rows = Integer.parseInt(dimensions[0]);
        int cols = Integer.parseInt(dimensions[1]);
        String[] values;
        int col = 0;
        int row = 0;
        int sum = 0;
        int[][] maze = new int[rows][cols];
        for(int i = 0; i < maze.length; i++){
            values = scanner.nextLine().split(" ");
            for(int j = 0; j < maze[i].length; j++){
                maze[i][j] = Integer.parseInt(values[j]);
                if(maze[i][j] == 0){
                    row = i;
                    col = j;
                }
            }
        }

        System.out.println(exitLabyrinth(maze, row, col, sum));
    }

    private static int exitLabyrinth(int[][] maze, int row, int col, int sum){
        int maxNumberOfCoins = -1;
        int updateRow = -1;
        int updateCol = -1;

        if(inLabyrinthBounds(maze, row, col -1)){
            if(maze[row][col-1] >= maxNumberOfCoins){
                maxNumberOfCoins = maze[row][col-1];
                updateRow = row;
                updateCol = col -1;
            }
        }

        if(inLabyrinthBounds(maze, row, col+1)){
            if(maze[row][col+1] > maxNumberOfCoins){
                maxNumberOfCoins = maze[row][col+1];
                updateRow = row;
                updateCol = col+1;
            }
        }

        if(inLabyrinthBounds(maze, row -1, col)){
            if(maze[row-1][col] > maxNumberOfCoins){
                maxNumberOfCoins = maze[row-1][col];
                updateRow = row-1;
                updateCol = col;
            }
        }

        if(inLabyrinthBounds(maze, row +1, col)){
            if(maze[row+1][col] > maxNumberOfCoins){
                maxNumberOfCoins = maze[row+1][col];
                updateRow = row+1;
                updateCol = col;
            }
        }

        if(maxNumberOfCoins > 0){
            sum++;
            row = updateRow;
            col = updateCol;
            maze[row][col] -= 1;
            return exitLabyrinth(maze, row, col, sum);
        }

        return sum;
    }

    private static boolean inLabyrinthBounds(int[][] maze, int row, int col){
        return row >= 0 && col >= 0 && row < maze.length && col < maze[row].length;
    }
}