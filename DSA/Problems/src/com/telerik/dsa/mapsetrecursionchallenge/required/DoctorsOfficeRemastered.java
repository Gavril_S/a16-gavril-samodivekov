package com.telerik.dsa.mapsetrecursionchallenge.required;

import java.util.*;

public class DoctorsOfficeRemastered {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<String> patients = new ArrayList<>();
        Map<String, Integer> peopleCount = new HashMap<>();
//        List<String> list = new ArrayList<>();
        String[] input;

        while(true){
            input = scanner.nextLine().split(" ");

            if(input[0].equalsIgnoreCase("End")){
                break;
            }

            if(input[0].equalsIgnoreCase("Append")){
                patients.add(input[1]);
                addPersonToMap(peopleCount, input[1]);
//                list.add("OK");
                System.out.println("OK");
            }

            if(input[0].equalsIgnoreCase("Insert")){
                if(Integer.parseInt(input[1]) < 0 || Integer.parseInt(input[1]) > patients.size()){
//                    list.add("Error");
                    System.out.println("Error");
                } else {
                    patients.add(Integer.parseInt(input[1]), input[2]);
                    addPersonToMap(peopleCount, input[2]);
//                    list.add("OK");
                    System.out.println("OK");
                }
            }

            if(input[0].equalsIgnoreCase("Find")){
//                list.add(peopleCount.getOrDefault(input[1], 0).toString());
                System.out.println(peopleCount.getOrDefault(input[1], 0).toString());
            }

            if(input[0].equalsIgnoreCase("Examine")){
                if(Integer.parseInt(input[1]) > patients.size()){
//                    list.add("Error");
                    System.out.println("Error");
                } else {
                    StringBuilder strBuilder = new StringBuilder();
                    for(int i = 0; i < Integer.parseInt(input[1]); i++){
                        if(peopleCount.get(patients.get(0)) > 1){
                            peopleCount.put(patients.get(0), peopleCount.get(patients.get(0)) - 1);
                        } else {
                            peopleCount.remove(patients.get(0));
                        }
                        strBuilder.append(patients.get(0));
                        strBuilder.append(" ");
                        patients.remove(0);
                    }
                    strBuilder.setLength(strBuilder.length() - 1);
//                    list.add(strBuilder.toString());
                    System.out.println(strBuilder.toString());
                }
            }
        }

//        for (String elem : list) {
//            System.out.println(elem);
//        }
    }

    private static void addPersonToMap(Map<String, Integer> peopleCount, String key) {
        if(peopleCount.containsKey(key)){
            peopleCount.put(key, peopleCount.get(key) + 1);
        } else {
            peopleCount.put(key, 1);
        }
    }
}