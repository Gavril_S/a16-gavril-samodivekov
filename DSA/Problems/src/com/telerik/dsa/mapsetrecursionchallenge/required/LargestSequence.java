package com.telerik.dsa.mapsetrecursionchallenge.required;

import java.util.Scanner;

public class LargestSequence {
    private static final char VISITED = '\u25CD';

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] initialInput = scanner.nextLine().split(" ");
        int row = Integer.parseInt(initialInput[0]);
        int col = Integer.parseInt(initialInput[1]);
        int[][] matrix = new int[row][col];
        String[] values;
        for(int i = 0; i < matrix.length; i++){
            values = scanner.nextLine().split(" ");
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = Integer.parseInt(values[j]);
            }
        }
        System.out.println(getLargestSequence(matrix));
    }

    private static int getLargestSequence(int[][] matrix){
        if(matrix.length == 0){
            return 0;
        }
        int maxSequence = 1;
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                if(matrix[i][j] == VISITED){
                    continue;
                }
                int value = matrix[i][j];
                int sequence = calculateSequence(matrix, i, j, value);
                if(sequence > maxSequence){
                    maxSequence = sequence;
                }
            }
        }
        return maxSequence;
    }

    private static int calculateSequence(int[][] matrix, int row, int col, int value){
        if(!inBounds(matrix, row, col, value)){
            return 0;
        }
        if(matrix[row][col] == VISITED){
            return 0;
        }
        matrix[row][col] = VISITED;
        return 1 +
                calculateSequence(matrix, row + 1, col, value) +
                calculateSequence(matrix, row, col +1, value) +
                calculateSequence(matrix, row - 1, col, value) +
                calculateSequence(matrix, row, col - 1, value);
    }

    private static boolean inBounds(int[][] matrix, int row, int col, int value){
        return row >= 0 && col >= 0 && row < matrix.length && col < matrix[row].length && matrix[row][col] == value;
    }
}