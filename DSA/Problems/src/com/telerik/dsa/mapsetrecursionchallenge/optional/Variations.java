package com.telerik.dsa.mapsetrecursionchallenge.optional;

import java.util.Scanner;

public class Variations {
    public static void main(String[] args) {
        Scanner scanner = new Scanner((System.in));
        int z = scanner.nextInt();
        scanner.nextLine();
        String[] inputString = scanner.nextLine().split(" ");
        String x = inputString[0];
        String y = inputString[1];
        String[] inputArr = new String[2];
        if(x.compareTo(y) >= 0){
            inputArr[0] = y;
            inputArr[1] = x;
        } else {
            inputArr[0] = x;
            inputArr[1] = y;
        }
        String input = inputArr[0] + inputArr[1];
        printVariations(input, z, new StringBuilder());
    }

    private static void printVariations(String input, int z, StringBuilder output){
        if(z == 0){
            System.out.println(output);
        } else {
            for (int i = 0; i < input.length(); i++) {
                output.append(input.charAt(i));
                printVariations(input, z - 1, output);
                output.deleteCharAt(output.length() - 1);
            }
        }
    }
}
