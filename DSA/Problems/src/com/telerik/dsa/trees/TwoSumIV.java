package com.telerik.dsa.trees;

import java.util.HashSet;
import java.util.Set;

public class TwoSumIV {

    public boolean findTarget(TreeNode root, int k) {
        Set<Integer> set = new HashSet<>();
        Set<Boolean> flag = new HashSet<>();
        dfs(root, k, set, flag);

        return flag.contains(true);
    }

    private void dfs(TreeNode root, int k, Set<Integer> set, Set<Boolean> flag){
        if(root == null){
            return;
        }
        if(set.contains(k - root.val)){
            flag.add(true);
            return;
        } else {
            set.add(root.val);
        }
        dfs(root.left, k, set, flag);
        dfs(root.right, k, set, flag);

    }
}
