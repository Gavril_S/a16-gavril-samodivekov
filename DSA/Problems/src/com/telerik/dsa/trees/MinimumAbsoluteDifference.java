package com.telerik.dsa.trees;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimumAbsoluteDifference {

    public int getMinimumDifference(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        int minAbsoluteDifference = Integer.MAX_VALUE;
        dfs(root, list);

        Collections.sort(list);
        for (int i = 0; i < list.size()-1; i++) {
            int currentMinValue = Math.abs(list.get(i) - list.get(i+1));
            if(currentMinValue < minAbsoluteDifference){
                minAbsoluteDifference = currentMinValue;
            }
        }
        return minAbsoluteDifference;
    }

    private void dfs(TreeNode root, List<Integer> list){
        if(root == null){
            return;
        }
        dfs(root.left, list);
        list.add(root.val);
        dfs(root.right, list);
    }
}
