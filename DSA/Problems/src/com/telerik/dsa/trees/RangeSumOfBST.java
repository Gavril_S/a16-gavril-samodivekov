package com.telerik.dsa.trees;

import java.util.ArrayList;
import java.util.List;

class RangeSumOfBST{

    public int rangeSumBST(TreeNode root, int L, int R) {
        List<Integer> list = new ArrayList<>();
        int sum = 0;
        getElements(root, list);
        for (Integer elem : list) {
            if(elem > L && elem < R){
                sum += elem;
            }
        }
        sum += (L + R);
        return sum;
    }

    private void getElements(TreeNode root, List<Integer> list){
        if(root == null){
            return;
        }
        getElements(root.left, list);
        getElements(root.right, list);
        list.add(root.val);
    }
}