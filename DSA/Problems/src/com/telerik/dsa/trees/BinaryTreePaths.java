package com.telerik.dsa.trees;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePaths {

    public List<String> binaryTreePaths(TreeNode root) {
        List<String> list = new ArrayList<>();
        collectPaths(root, list, "");

        return list;
    }

    private void collectPaths(TreeNode root, List<String> list, String path){
        if(root == null){
            return;
        }
        path += root.val;
        if(root.left == null && root.right == null){
            list.add(path);
            return;
        }
        if(root.left != null){
            collectPaths(root.left, list, path + "->");
        }
        if(root.right != null){
            collectPaths(root.right, list, path + "->");
        }
    }
}
