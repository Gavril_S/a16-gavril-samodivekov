package com.telerik.dsa.finalpractice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SmallWorld {
    private static final char VISITED = '\u0446';

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] input = reader.readLine().split(" ");
        int rows = Integer.parseInt(input[0]);
        int cols = Integer.parseInt(input[1]);
        int[][] board = new int[rows][cols];
        String[] values;
        for (int i = 0; i < board.length; i++) {
            values = reader.readLine().split("");
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = Integer.parseInt(values[j]);
            }
        }
        List<Integer> conquestSizes = new ArrayList<>();
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[row].length; col++) {
                if (board[row][col] != 1) {
                    continue;
                }
                int currentConquestSize = conquest(board, row, col);
                conquestSizes.add(currentConquestSize);
            }
        }
        conquestSizes.stream()
                .sorted(Comparator.reverseOrder())
                .forEach(elem -> {
                    System.out.println(elem);
                });
    }

    private static int conquest(int[][] board, int row, int col) {
        if (outOFBounds(board, row, col) || board[row][col] != 1) {
            return 0;
        }
        board[row][col] = VISITED;

        return 1 +
                conquest(board, row + 1, col) +
                conquest(board, row, col + 1) +
                conquest(board, row - 1, col) +
                conquest(board, row, col - 1);
    }

    private static boolean outOFBounds(int[][] board, int row, int col) {
        return row < 0 || col < 0 || row >= board.length || col >= board[row].length;
    }
}