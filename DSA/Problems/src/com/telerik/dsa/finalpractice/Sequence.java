package com.telerik.dsa.finalpractice;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Sequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int n = scanner.nextInt();

        Queue<Integer> values = new LinkedList<>();
        int currentValue = k;
        int counter = 1;

        for (int i = 0; i < n - 1; i++) {
            if (counter == 1) {
                currentValue = k + 1;
                values.offer(currentValue);
                counter++;
            } else if (counter == 2) {
                currentValue = 2 * k + 1;
                values.offer(currentValue);
                counter++;
            } else if (counter == 3) {
                currentValue = k + 2;
                values.offer(currentValue);
                counter = 1;
                k = values.poll();
            }
        }
        System.out.println(currentValue);
    }
}