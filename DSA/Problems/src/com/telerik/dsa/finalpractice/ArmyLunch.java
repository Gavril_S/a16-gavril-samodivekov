package com.telerik.dsa.finalpractice;

import java.util.*;

public class ArmyLunch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfSoldiers = scanner.nextInt();
        scanner.nextLine();
        String[] soldiersInput = scanner.nextLine().split(" ");

        Map<Integer, String> soldiers = new LinkedHashMap<>();
        int counter = 0;

        for (int i = 0; i < soldiersInput.length; i++) {
            soldiers.put(counter, soldiersInput[i]);
            counter++;
        }

        StringBuilder sb = new StringBuilder();
        StringBuilder cb = new StringBuilder();
        StringBuilder pb = new StringBuilder();

        for (int i = 0; i < soldiers.size(); i++) {
            if(soldiers.get(i).charAt(0) == 'S'){
                sb.append(soldiers.get(i) + " ");
            } else if(soldiers.get(i).charAt(0) == 'C'){
                cb.append(soldiers.get(i) + " ");
            } else {
                pb.append(soldiers.get(i) + " ");
            }
        }

        System.out.printf("%s%s%s", sb.toString(), cb.toString(), pb.toString().trim());
    }
}
