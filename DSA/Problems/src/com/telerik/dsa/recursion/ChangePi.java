package com.telerik.dsa.recursion;

import java.util.Scanner;

public class ChangePi {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();
        StringBuilder strBuild = new StringBuilder();

        System.out.println(changePi(str, strBuild));
    }

    private static String changePi(String str, StringBuilder strBuild){
        if(str.length() < 3){
            if(str.length() == 0){
                return strBuild.toString().trim();
            } else if(str.length() == 1){
                strBuild.append(str.charAt(0));
                return strBuild.toString().trim();
            }

            if(str.charAt(0) == 'p' && str.charAt(1) == 'i'){
                strBuild.append("3.14");
            } else {
                strBuild.append(str);
            }
            return strBuild.toString().trim();
        }

        if(str.charAt(0) == 'p' && str.charAt(1) == 'i'){
            strBuild.append("3.14");
            return changePi(str.substring(2), strBuild);
        }

        strBuild.append(str.charAt(0));
        return changePi(str.substring(1), strBuild);
    }
}
