package com.telerik.dsa.recursion;

import java.util.Scanner;

public class Count {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int count = 0;

        System.out.println(findOccurrencesOfSeven(n, count));
    }

    private static int findOccurrencesOfSeven(int n, int count){
        if(n == 0){
            return count;
        } else if(n < 10){
            if(n == 7){
                return count+=1;
            } else {
                return count;
            }
        }

        if(n%10 == 7){
            count++;
        }

        return findOccurrencesOfSeven(n/10, count);
    }
}
