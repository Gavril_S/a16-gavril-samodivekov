package com.telerik.dsa.recursion;

import java.util.Scanner;

public class ArrayTwoTwenty {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(",");
        int[] arr = new int[input.length];

        for(int i=0;i<input.length;i++){
            arr[i] = Integer.parseInt(input[i]);
        }

        int index = scanner.nextInt();

        System.out.println(searchArray(arr, index));
    }

    private static boolean searchArray(int[] arr, int index){
        if(index >= arr.length-1){
            return false;
        }

        if(arr[index]*10 == arr[index+1]){
            return true;
        }

        return searchArray(arr, index+1);
    }
}
