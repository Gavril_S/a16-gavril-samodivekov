package com.telerik.dsa.recursion;

import java.util.Scanner;

public class SumDigits {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int remainder = 0;

        System.out.println(findDigitSum(n, remainder));
    }

    private static int findDigitSum(int n, int remainder){
        if(n == 0){
            return remainder;
        } else if(n < 10){
            return remainder += n;
        }
        remainder += n % 10;
        return findDigitSum(n/10, remainder);
    }
}
