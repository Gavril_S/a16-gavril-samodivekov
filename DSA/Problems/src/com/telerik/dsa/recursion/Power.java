package com.telerik.dsa.recursion;

public class Power {
    public static void main(String[] args) {
        int base = 9;
        int exponent = 2;

        System.out.println(powers(base, exponent));
    }

    public static int powers(int b, int e){
        if(e == 0){
            return 1;
        }

        return b *powers(b, e -1);
    }
}
