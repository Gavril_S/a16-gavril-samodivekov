package com.telerik.dsa.recursion;

import java.util.Scanner;

public class CountX {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        int count = 0;

        System.out.println(countX(input, count));
    }

    private static int countX(String input, int count){
        if(input.length() < 2){
            if(input.length() == 0){
                return count;
            }

            if(input.charAt(0) == 'x'){
                count+=1;
            }
            return count;
        }

        if(input.charAt(0) == 'x'){
            count++;
        }

        return countX(input.substring(1), count);
    }
}
