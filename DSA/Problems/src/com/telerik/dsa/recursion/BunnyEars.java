package com.telerik.dsa.recursion;

import java.util.Scanner;

public class BunnyEars {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int bunnyEars = 0;

        System.out.println(countBunnyEars(n, bunnyEars));
    }

    private static int countBunnyEars(int n, int bunnyEars){
        if(n < 1){
            return bunnyEars;
        }
        bunnyEars += 2;
        return countBunnyEars(n - 1, bunnyEars);
    }
}
