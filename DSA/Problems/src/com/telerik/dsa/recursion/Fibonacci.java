package com.telerik.dsa.recursion;

import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        long[] memorisedValues = new long[n + 1];

        System.out.println(findFibonacci(n, memorisedValues));
    }

    private static int findFibonacci(int n, long[] memorisedValues) {
        if(n == 0){
            return (int) memorisedValues[n];
        }
        if (n == 1 || n == 2) {
            memorisedValues[n] = 1;
        } else {
            memorisedValues[n] = findFibonacci(n - 1, memorisedValues) + findFibonacci(n - 2, memorisedValues);
        }
        return (int) memorisedValues[n];
    }

}

//    private static int findFibonacci(int n, long[] memorisedValues){
//        if(n<1){
//            return 0;
//        } else {
//            if(n == 1 || n == 2) {
//                return 1;
//            } else {
//                memorisedValues[n] = findFibonacci(n-1, memorisedValues) + findFibonacci(n-2, memorisedValues);
//            }
//        }
//        return (int)memorisedValues[n];
//    }
//}
