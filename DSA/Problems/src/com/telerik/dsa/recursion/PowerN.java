package com.telerik.dsa.recursion;

import java.util.Scanner;

public class PowerN {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int base = scanner.nextInt();
        int exponent = scanner.nextInt();

        System.out.println(findPower(base, exponent));
    }

    private static int findPower(int b, int e){
        if(e == 0){
            return 1;
        }

        return b*findPower(b, e-1);
    }
}
