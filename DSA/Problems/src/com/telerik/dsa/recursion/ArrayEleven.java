package com.telerik.dsa.recursion;

import java.util.Scanner;

public class ArrayEleven {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(",");
        int[] arr = new int[input.length];

        for(int i=0;i<input.length;i++){
            arr[i] = Integer.parseInt(input[i]);
        }

        int index = scanner.nextInt();
        int count = 0;

        System.out.println(checkForSix(arr, index, count));
    }

    private static int checkForSix(int[] arr, int index, int count){
        if(index >= arr.length){
            return count;
        }

        if(arr[index] == 11){
            count++;
        }

        return checkForSix(arr, index + 1, count);
    }
}
