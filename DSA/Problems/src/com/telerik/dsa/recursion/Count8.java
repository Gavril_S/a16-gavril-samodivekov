package com.telerik.dsa.recursion;

import java.util.Scanner;

public class Count8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int count = 0;
        int checker = 0;

        System.out.println(findCount(n, count, checker));
    }

    private static int findCount(int n, int count, int checker){
        if(n == 0){
            return count;
        } else if(n < 10){
            if(n == 8){
                return count+=1;
            } else {
                return count;
            }
        }

        if(n%10 == 8){
            checker = n/10;
            if(checker % 10 == 8){
                count+=2;
            } else {
                count++;
            }
        }
        return findCount(n/10, count, 0);
    }
}
