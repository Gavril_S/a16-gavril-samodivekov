package com.telerik.dsa.recursion;

import java.util.Scanner;

public class ArraySix {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(",");
        int[] arr = new int[input.length];

        for(int i=0;i<input.length;i++){
            arr[i] = Integer.parseInt(input[i]);
        }

        int index = scanner.nextInt();

        System.out.println(checkForSix(arr, index));
    }

    private static boolean checkForSix(int[] arr, int index){
        if(index >= arr.length){
            return false;
        }

        if(arr[index] == 6){
            return true;
        }

        return checkForSix(arr, index + 1);
    }
}
