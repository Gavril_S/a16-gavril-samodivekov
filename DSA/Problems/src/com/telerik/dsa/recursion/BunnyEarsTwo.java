package com.telerik.dsa.recursion;

import java.util.Scanner;

public class BunnyEarsTwo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int bunnyEarsCount = 0;

        System.out.println(findBunnyEars(n, bunnyEarsCount));
    }

    private static int findBunnyEars(int n, int bunnyEarsCount){
        if(n < 1){
            return bunnyEarsCount;
        } else if(n%2 == 0){
            bunnyEarsCount += 3;
        } else {
            bunnyEarsCount += 2;
        }

        return findBunnyEars(n-1, bunnyEarsCount);
    }
}
