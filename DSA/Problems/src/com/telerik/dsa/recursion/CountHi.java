package com.telerik.dsa.recursion;

import java.util.Scanner;

public class CountHi {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();
        int count = 0;

        System.out.println(countHi(str, count));
    }

    private static int countHi(String str, int count){
        if(str.length() < 3){
            if(str.length() == 0 || str.length() == 1){
                return count;
            }

            if(str.charAt(0) == 'h' && str.charAt(1) == 'i'){
                count+=1;
            }
            return count;
        }

        if(str.charAt(0) == 'h' && str.charAt(1) == 'i'){
            count+=1;
        }

        return countHi(str.substring(1), count);
    }
}
