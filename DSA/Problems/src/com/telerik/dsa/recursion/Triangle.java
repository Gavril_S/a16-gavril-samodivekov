package com.telerik.dsa.recursion;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int blockNumber = 0;

        System.out.println(findNumberOfBlocks(n, blockNumber));
    }

    private static int findNumberOfBlocks(int n, int blockNum){
        if(n == 0){
            return blockNum;
        }

        blockNum += n;

        return findNumberOfBlocks(n-1, blockNum);
    }
}
