package com.telerik.dsa.setandmap;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class JewelsAndStones {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String j = scanner.nextLine();
        String s = scanner.nextLine();

        System.out.println(numJewelsInStones(j, s));
    }

    public static int numJewelsInStones(String J, String S) {
        Set<Character> jewels = new HashSet<>();
        int count = 0;
        for (int i = 0; i < J.length(); i++) {
            jewels.add(J.charAt(i));
        }
        for (int i = 0; i < S.length(); i++) {
            if(jewels.contains(S.charAt(i))){
                count++;
            }
        }
        return count;
    }
}
