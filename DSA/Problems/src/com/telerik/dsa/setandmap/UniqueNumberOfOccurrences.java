package com.telerik.dsa.setandmap;

import java.util.*;

public class UniqueNumberOfOccurrences {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(",");
        int[] arr = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            arr[i] = Integer.parseInt(input[i]);
        }

        System.out.println(uniqueOccurrences(arr));
    }

    public static boolean uniqueOccurrences(int[] arr) {
        Map<Integer, Integer> myMap = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            if(!myMap.containsKey(arr[i])){
                myMap.put(arr[i], 1);
            } else {
                myMap.put(arr[i], myMap.get(arr[i]) + 1);
            }
        }

        Set<Integer> mySet = new HashSet<>();
        mySet.addAll(myMap.values());

        return myMap.size() == mySet.size();
    }
}