package com.telerik.dsa.setandmap;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SetMissmatch {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(",");
        int[] nums = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            nums[i] = Integer.parseInt(input[i]);
        }
        int[] output = findErrorNums(nums);
        System.out.printf("[%d,%d]", output[0], output[1]);
    }

    public static int[] findErrorNums(int[] nums) {
        Set<Integer> mySet = new HashSet<>();
        int duplicate = 0;
        int missingNum = 0;
        for (int i = 0; i < nums.length; i++) {
            if(mySet.contains(nums[i])){
                duplicate = nums[i];
            } else {
                mySet.add(nums[i]);
            }
        }

        for (int i = 1; i <= nums.length; i++) {
            if(!mySet.contains(i)){
                missingNum = i;
                break;
            }
        }
        int[] output = {duplicate, missingNum};
        return output;
    }
}