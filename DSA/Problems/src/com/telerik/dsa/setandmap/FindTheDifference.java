package com.telerik.dsa.setandmap;

import java.util.*;

public class FindTheDifference {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        String t = scanner.nextLine();

        System.out.println(findTheDifference(s, t));
    }

    public static char findTheDifference(String s, String t) {
        Map<Character, String> map1 = new HashMap<>();
        Map<Character, String> map2 = new HashMap<>();

        for (int i = 0; i < t.length(); i++) {
            if(i < t.length() - 1){
                if(!map1.containsKey(s.charAt(i))){
                    map1.put(s.charAt(i), "" + s.charAt(i));
                } else {
                    map1.put(s.charAt(i), map1.get(s.charAt(i)) + s.charAt(i));
                }
            }
            
            if(!map2.containsKey(t.charAt(i))){
                map2.put(t.charAt(i), "" + t.charAt(i));
            } else {
                map2.put(t.charAt(i), map2.get(t.charAt(i)) + t.charAt(i));
            }
        }

        Set<Character> list1 = new HashSet<>();
        List<Character> list2 = new ArrayList<>();
        list1.addAll(map1.keySet());
        list2.addAll(map2.keySet());
        if(list1.size() != list2.size()){
            for (int i = 0; i < list2.size(); i++) {
                if(!list1.contains(list2.get(i))){
                    return list2.get(i);
                }
            }
        }
        Set<String> strSet = new HashSet<>();
        List<String> strList = new ArrayList<>();
        strSet.addAll(map1.values());
        strList.addAll(map2.values());
        char returnValue = ' ';
        for (int i = 0; i < strList.size(); i++) {
            if(!strSet.contains(strList.get(i))){
                returnValue = strList.get(i).charAt(0);
            }
        }
        return returnValue;
    }
}