package com.telerik.dsa.setandmap;

import java.util.*;

public class UncommonWords {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String a = scanner.nextLine();
        String b = scanner.nextLine();

        System.out.println(Arrays.toString(uncommonFromSentences(a, b)));
    }

    public static String[] uncommonFromSentences(String A, String B) {
        String[] sentence1 = A.split(" ");
        String[] sentence2 = B.split(" ");

        Set<String> set1 = new HashSet<>();
        Set<String> set2 = new HashSet<>();
        List<String> repeatedWords = new ArrayList<>();
        int size;
        if (sentence1.length >= sentence2.length) {
            size = sentence1.length;
        } else {
            size = sentence2.length;
        }

        for (int i = 0; i < size; i++) {
            addingElementsToSet(sentence1, set1, repeatedWords, i);
            addingElementsToSet(sentence2, set2, repeatedWords, i);
        }

        set1.removeAll(repeatedWords);
        set2.removeAll(repeatedWords);
        for (String elem : set2) {
            if (set1.contains(elem)) {
                set1.remove(elem);
            } else {
                set1.add(elem);
            }
        }

//        String[] uncommonWords = set1.stream().toArray(String[]::new);
        List<String> list = new ArrayList<>();
        list.addAll(set1);
        String[] uncommonWords = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            uncommonWords[i] = list.get(i);
        }
        return uncommonWords;
    }

    private static void addingElementsToSet(String[] sentence, Set<String> set, List<String> repeatedWords, int i) {
        if (i < sentence.length) {
            if (!set.contains(sentence[i])) {
                set.add(sentence[i]);
            } else {
                repeatedWords.add(sentence[i]);
            }
        }
    }
}
