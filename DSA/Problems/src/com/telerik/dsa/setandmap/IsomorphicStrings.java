package com.telerik.dsa.setandmap;

import java.util.*;

public class IsomorphicStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        String t = scanner.nextLine();

        System.out.println(isIsomorphic(s, t));
    }

    public static boolean isIsomorphic(String s, String t) {
        if(s.length() != t.length()){
            return false;
        }

        Map<Character, Character> mapS = new LinkedHashMap<>();
        Map<Character, Character> mapT = new LinkedHashMap<>();

        for (int i = 0; i < s.length(); i++) {
            if(!mapS.containsKey(s.charAt(i)) && mapT.containsKey(t.charAt(i))){
                return false;
            } else if(mapS.containsKey(s.charAt(i)) && !mapT.containsKey(t.charAt(i))){
                return false;
            } else if(mapS.containsKey(s.charAt(i)) && mapT.containsKey(t.charAt(i))){
                if(mapS.get(s.charAt(i)) != mapT.get(t.charAt(i))){
                    return false;
                }
            } else if(!mapS.containsKey(s.charAt(i)) && !mapT.containsKey(t.charAt(i))){
                mapS.put(s.charAt(i), s.charAt(i));
                mapT.put(t.charAt(i), s.charAt(i));
            }
        }
        boolean flag = true;
        List<Character> list1 = new ArrayList<>();
        List<Character> list2 = new ArrayList<>();
        list1.addAll(mapS.values());
        list2.addAll(mapT.values());
        for (int i = 0; i < list1.size(); i++) {
            if(list1.get(i) != list2.get(i)){
                flag = false;
                break;
            }
        }

        return flag;
    }
}