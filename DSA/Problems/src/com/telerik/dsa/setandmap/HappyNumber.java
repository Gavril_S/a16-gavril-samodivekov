package com.telerik.dsa.setandmap;

import java.util.*;

public class HappyNumber {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        System.out.println(isHappy(n));
    }

    public static boolean isHappy(int n) {
        List<Integer> list = new ArrayList<>();
        int originalInput = n;
        while(n != 1 ){
            list.clear();
            if(n >= 10){
                while (n >= 10){
                    list.add(n % 10);
                    n /= 10;
                }
                n *= n;
                for (int k = 0; k < list.size(); k++) {
                    n += list.get(k) * list.get(k);
                }
            } else {
                n *= n;
            }
            if(n == originalInput){
                break;
            }
        }
        return true;
    }
}
