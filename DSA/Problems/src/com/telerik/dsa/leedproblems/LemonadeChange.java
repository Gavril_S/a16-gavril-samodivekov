package com.telerik.dsa.leedproblems;

import java.util.Scanner;

public class LemonadeChange {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(",");
        int[] intArray = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            intArray[i] = Integer.parseInt(input[i]);
        }

        System.out.println(lemonadeChange(intArray));
    }

    private static boolean lemonadeChange(int[] bills) {
        int counterFive = 0;
        int counterTen = 0;
        boolean checker = true;

        for (int i = 0; i < bills.length; i++){
            if(bills[i] == 5){
                counterFive++;
            } else if(bills[i] == 10){
                if(counterFive > 0){
                    counterFive--;
                    counterTen++;
                } else {
                    checker = false;
                    break;
                }
            } else {
                if(counterTen > 0){
                    if(counterFive > 0){
                        counterFive--;
                        counterTen--;
                    } else {
                        checker = false;
                        break;
                    }
                } else if(counterTen <= 0){
                    if(counterFive >= 3){
                        counterFive -= 3;
                    } else {
                        checker = false;
                        break;
                    }
                }
            }
        }
        return checker;
    }
}
