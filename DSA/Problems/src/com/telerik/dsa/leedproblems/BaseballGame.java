package com.telerik.dsa.leedproblems;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

public class BaseballGame {
    private static final int NUMBER_OF_ROUNDS_TO_GET_VALUES_FROM = 2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String [] input = scanner.nextLine().split(",");
        System.out.println(calPoints(input));
    }

    public static int calPoints(String[] ops) {
        Stack<Integer> values = new Stack<>();
        int twoValidRoundsSum;
        int tempValueHolder;
        int sum = 0;

        for(int i = 0; i < ops.length; i++){
            String element = ops[i];
            if(!element.equals("+") && !element.equals("C") && !element.equals("D")){
                values.push(Integer.parseInt(element));
            } else if(element.equals("+")){
                if(values.size() >= NUMBER_OF_ROUNDS_TO_GET_VALUES_FROM){
                    tempValueHolder = values.pop();
                    twoValidRoundsSum = tempValueHolder + values.peek();
                    values.push(tempValueHolder);
                    values.push(twoValidRoundsSum);
                } else if(values.size() == NUMBER_OF_ROUNDS_TO_GET_VALUES_FROM - 1){
                    twoValidRoundsSum = values.peek();
                    values.push(twoValidRoundsSum);
                } else {
                    values.push(0);
                }
            } else if(element.equals("D")){
                tempValueHolder = values.peek() * 2;
                values.push(tempValueHolder);
            } else if(element.equals("C")){
                values.pop();
            }
        }

        Iterator<Integer> valuesIterator = values.iterator();
        while(valuesIterator.hasNext()){
            sum += valuesIterator.next();
        }
        return sum;
    }
}
