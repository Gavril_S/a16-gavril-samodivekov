package com.telerik.dsa.leedproblems;

import java.util.Scanner;
import java.util.Stack;

public class ValidParanteses {

    public static void main(String[] args) {
        // write your code here
        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();

        System.out.println(isValid(str));
    }

    private static boolean isValid(String s) {
        Stack<Character> indices = new Stack<>();
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if(c == '('){
                indices.push(c);
            } else if(c == ')'){
                if(indices.size() > 0 && indices.peek() == '('){
                    indices.pop();
                } else {
                    indices.push(s.charAt(i));
                }
            }
            if(c == '{'){
                indices.push(c);
            } else if(c == '}'){
                if(indices.size() > 0 && indices.peek() == '{'){
                    indices.pop();
                } else {
                    indices.push(s.charAt(i));
                }
            }
            if(c == '['){
                indices.push(c);
            } else if(c == ']'){
                if(indices.size() > 0 && indices.peek() == '['){
                    indices.pop();
                } else {
                    indices.push(s.charAt(i));
                }
            }
        }
        return indices.empty();
    }
}
