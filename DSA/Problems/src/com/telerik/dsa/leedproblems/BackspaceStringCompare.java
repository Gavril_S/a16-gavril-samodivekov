package com.telerik.dsa.leedproblems;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

public class BackspaceStringCompare {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //enter the input
        String str1 = scanner.nextLine();
        String str2 = scanner.nextLine();

        System.out.println(backspaceCompare(str1, str2));
    }

    public static boolean backspaceCompare(String S, String T) {
        String str1Result = backspaceCompare(S);
        String str2Result = backspaceCompare(T);
        return str1Result.equals(str2Result);
    }

    private static String backspaceCompare(String str){
        StringBuilder builder = new StringBuilder();
        Stack<Character> stack = new Stack<>();
        Stack<Character> stack2 = new Stack<>();
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i)!= '#'){
                stack.push(str.charAt(i));
            } else if(stack.size() > 0 && str.charAt(i) == '#'){
                stack.pop();
            }
        }

        while(!stack.empty()){
            stack2.push(stack.pop());
        }
        Iterator<Character> stack2Iterator = stack2.iterator();
        while (stack2Iterator.hasNext()) {
            builder.append(stack2Iterator.next());
        }

        return builder.toString().trim();
    }
}
