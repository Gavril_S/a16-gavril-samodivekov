package com.telerik.oop.hms;

public class Patient extends User{

    private boolean insured;

    public Patient(long patientId, String firstName, String lastName, String gender, String email, boolean insured) {
        super(patientId, firstName, lastName, gender, email);
        setInsured(insured);
    }

    public boolean isInsured() {
        return insured;
    }

    private void setInsured(boolean insured) {
        this.insured = insured;
    }
}
