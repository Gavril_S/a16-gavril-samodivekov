package com.telerik.oop.hms;

public class Doctor extends Staff {

    private String specialisation;

    public Doctor(long doctorId, String firstName, String lastName, String gender, String email, int yearsOfExperience, String description, double salary, String specialisation) {
        super(doctorId, firstName, lastName, gender, email, yearsOfExperience, description, salary);
        setSpecialisation(specialisation);
    }

    public String getSpecialisation() {
        return specialisation;
    }

    private void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }
}
