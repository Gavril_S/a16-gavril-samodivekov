package com.telerik.oop.hms;

public class User {

    private long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;

    public User(long id, String firstName, String lastName, String gender, String email) {
       setId(id);
       setFirstName(firstName);
       setLastName(lastName);
       setGender(gender);
       setEmail(email);
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    private void setId(long id) {
        this.id = id;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private void setGender(String gender) {
        this.gender = gender;
    }

    private void setEmail(String email) {
        this.email = email;
    }
}
