package com.telerik.oop.hms;

public class Staff extends User {

    private int yearsOfExperience;
    private String description;
    private double salary;

    public Staff(long staffId, String firstName, String lastName, String gender, String email, int yearsOfExperience, String description, double salary) {
        super(staffId, firstName, lastName, gender, email);
        setYearsOfExperience(yearsOfExperience);
        setDescription(description);
        setSalary(salary);
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public String getDescription() {
        return description;
    }

    public double getSalary() {
        return salary;
    }

    private void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setSalary(double salary) {
        this.salary = salary;
    }
}
