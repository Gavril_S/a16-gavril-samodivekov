package com.telerik.oop.hms.users;

import com.telerik.oop.hms.insurance.HealthInsurancePlan;

public class Nurse extends Staff {

    public Nurse(long nurseId, String firstName, String lastName, String gender, String email, int yearsOfExperience,
                    String description, double salary, boolean insured, HealthInsurancePlan insurancePlan) {
        super(nurseId, firstName, lastName, gender, email, yearsOfExperience, description, salary, insured, insurancePlan);
    }
}
