package com.telerik.oop.hms.users;

import com.telerik.oop.hms.insurance.HealthInsurancePlan;

public class Patient extends User {

    public Patient(long patientId, String firstName, String lastName, String gender, String email,
                        boolean insured, HealthInsurancePlan insurancePlan) {
        super(patientId, firstName, lastName, gender, email, insured, insurancePlan);

    }
}
