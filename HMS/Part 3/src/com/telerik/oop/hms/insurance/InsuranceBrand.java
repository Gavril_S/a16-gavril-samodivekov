package com.telerik.oop.hms.insurance;

public class InsuranceBrand {

    private long id;
    private String name;

    public InsuranceBrand(long id, String name){
        setId(id);
        setName(name);
    }

    public long getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    private void setId(long id){
        this.id = id;
    }

    private void setName(String name){
        this.name = name;
    }
}
