package com.telerik.oop.hms.insurance;

public class GoldPlan extends HealthInsurancePlan {

    private static final double GOLD_PLAN_COVERAGE = 0.8;
    private static final double GOLD_MONTHLY_PREMIUM_PERCENT = 0.07;

    public GoldPlan(long insBrandId, String insBrandName){
        super(GOLD_PLAN_COVERAGE, insBrandId, insBrandName);
    }

    public double computeMonthlyPremium(double salary){
        return salary * GOLD_MONTHLY_PREMIUM_PERCENT;
    }
}
