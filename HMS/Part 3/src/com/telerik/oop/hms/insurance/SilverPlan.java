package com.telerik.oop.hms.insurance;

public class SilverPlan extends HealthInsurancePlan {

    private static final double SILVER_PLAN_COVERAGE = 0.7;
    private static final double SILVER_MONTHLY_PREMIUM_PERCENT = 0.06;

    public SilverPlan(long insBrandId, String insBrandName){
        super(SILVER_PLAN_COVERAGE, insBrandId, insBrandName);
    }

    public double computeMonthlyPremium(double salary){
        return salary * SILVER_MONTHLY_PREMIUM_PERCENT;
    }
}
