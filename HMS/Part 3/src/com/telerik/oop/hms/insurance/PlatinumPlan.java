package com.telerik.oop.hms.insurance;

public class PlatinumPlan extends HealthInsurancePlan {

    private static final double PLATINUM_PLAN_COVERAGE = 0.9;
    private static final double PLATINUM_MONTHLY_PREMIUM_PERCENT = 0.08;

    public PlatinumPlan(long insBrandId, String insBrandName){
        super(PLATINUM_PLAN_COVERAGE, insBrandId, insBrandName);
    }

    public double computeMonthlyPremium(double salary){
        return salary * PLATINUM_MONTHLY_PREMIUM_PERCENT;
    }
}
