package com.telerik.oop.hms.insurance;

public class BronzePlan extends HealthInsurancePlan {

    private static final double BRONZE_PLAN_COVERAGE = 0.6;
    private static final double BRONZE_MONTHLY_PREMIUM_PERCENT = 0.05;

    public BronzePlan(long insBrandId, String insBrandName){
        super(BRONZE_PLAN_COVERAGE, insBrandId, insBrandName);
    }

    public double computeMonthlyPremium(double salary){
        return salary * BRONZE_MONTHLY_PREMIUM_PERCENT;
    }
}
