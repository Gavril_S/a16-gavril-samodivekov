package com.telerik.oop.hms.insurance;

public abstract class HealthInsurancePlan {

    private double coverage;
    private InsuranceBrand insBrand;

    public HealthInsurancePlan(double coverage, long insBrandId, String insBrandName){
        setCoverage(coverage);
        setInsBrand(insBrandId, insBrandName);
    }

    public abstract double computeMonthlyPremium(double salary);

    public double getCoverage(){
        return coverage;
    }

    public InsuranceBrand getInsBrand(){
        return insBrand;
    }

    private void setCoverage(double coverage){
        this.coverage = coverage;
    }

    private void setInsBrand(long brandId, String brandName){
        this.insBrand = new InsuranceBrand(brandId, brandName);
    }
}
