package com.telerik.oop.hms.insurance;

public class NoInsurancePlan extends HealthInsurancePlan {

    private static final int NO_INSURANCE_PLAN_COVERAGE = 0;
    private static final long NO_BRAND_ID = 0;
    private static final String NO_BRAND_NAME = "";


    public NoInsurancePlan(){
        super(NO_INSURANCE_PLAN_COVERAGE, NO_BRAND_ID, NO_BRAND_NAME);
    }

    public double computeMonthlyPremium(double salary){
        return NO_INSURANCE_PLAN_COVERAGE * salary;
    }
}
