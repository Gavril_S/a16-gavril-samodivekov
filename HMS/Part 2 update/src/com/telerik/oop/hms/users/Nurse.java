package com.telerik.oop.hms.users;

public class Nurse extends Staff {

    public Nurse(long nurseId, String firstName, String lastName, String gender, String email, int yearsOfExperience, String description, double salary) {
        super(nurseId, firstName, lastName, gender, email, yearsOfExperience, description, salary);
    }
}
