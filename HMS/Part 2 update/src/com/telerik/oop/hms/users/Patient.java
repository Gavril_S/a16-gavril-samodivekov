package com.telerik.oop.hms.users;

import com.telerik.oop.hms.insurance.HealthInsurancePlan;
import com.telerik.oop.hms.insurance.NoInsurancePlan;

public class Patient extends User {

    private boolean insured;
    private HealthInsurancePlan insurancePlan;

    public Patient(long patientId, String firstName, String lastName, String gender, String email,
                        boolean insured, HealthInsurancePlan insurancePlan) {
        super(patientId, firstName, lastName, gender, email);
        setInsured(insured);
        setInsurancePlan(insurancePlan);
    }

    public boolean isInsured() {
        return insured;
    }

    public HealthInsurancePlan getInsurancePlan(){
        return insurancePlan;
    }

    private void setInsured(boolean insured) {
        this.insured = insured;
    }

    private void setInsurancePlan(HealthInsurancePlan insurancePlan){
        if(!isInsured()){
            this.insurancePlan = new NoInsurancePlan();
        } else {
            this.insurancePlan = insurancePlan;
        }
    }
}
