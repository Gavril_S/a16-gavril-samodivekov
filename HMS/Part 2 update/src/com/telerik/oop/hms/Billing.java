package com.telerik.oop.hms;

import com.telerik.oop.hms.users.Patient;

public class Billing {
    private static final double BRONZE_PLAN_COVERAGE = 0.6;
    private static final double SILVER_PLAN_COVERAGE = 0.7;
    private static final double GOLD_PLAN_COVERAGE = 0.8;
    private static final double PLATINUM_PLAN_COVERAGE = 0.9;
    private static final int AMOUNT_PAID_BY_INS_COMPANY = 0;
    private static final int AMOUNT_PAID_BY_PATIENT = 1;
    private static final int DISCOUNT_WITHOUT_INS_PLAN = 20;
    private static final int DISCOUNT_WITH_BRONZE_PLAN = 25;
    private static final int DISCOUNT_WITH_SILVER_PLAN = 30;
    private static final int DISCOUNT_WITH_GOLD_PLAN = 40;
    private static final int DISCOUNT_WITH_PLATINUM_PLAN = 50;

    public static double[] computePaymentAmount(Patient patient, double amount){
        double[] payment = new double[2];
        if(!patient.isInsured()){
            payment[AMOUNT_PAID_BY_INS_COMPANY] = 0;
            payment[AMOUNT_PAID_BY_PATIENT] = amount - DISCOUNT_WITHOUT_INS_PLAN;
        } else if(patient.getInsurancePlan().getCoverage() == BRONZE_PLAN_COVERAGE){
            payment[AMOUNT_PAID_BY_INS_COMPANY] = amount * BRONZE_PLAN_COVERAGE;
            payment[AMOUNT_PAID_BY_PATIENT] = (amount - payment[AMOUNT_PAID_BY_INS_COMPANY]) - DISCOUNT_WITH_BRONZE_PLAN;
        } else if(patient.getInsurancePlan().getCoverage() == SILVER_PLAN_COVERAGE){
            payment[AMOUNT_PAID_BY_INS_COMPANY] = amount * SILVER_PLAN_COVERAGE;
            payment[AMOUNT_PAID_BY_PATIENT] = (amount - payment[AMOUNT_PAID_BY_INS_COMPANY]) - DISCOUNT_WITH_SILVER_PLAN;
        } else if(patient.getInsurancePlan().getCoverage() == GOLD_PLAN_COVERAGE){
            payment[AMOUNT_PAID_BY_INS_COMPANY] = amount * GOLD_PLAN_COVERAGE;
            payment[AMOUNT_PAID_BY_PATIENT] = (amount - payment[AMOUNT_PAID_BY_INS_COMPANY]) - DISCOUNT_WITH_GOLD_PLAN;
        } else if(patient.getInsurancePlan().getCoverage() == PLATINUM_PLAN_COVERAGE){
            payment[AMOUNT_PAID_BY_INS_COMPANY] = amount * PLATINUM_PLAN_COVERAGE;
            payment[AMOUNT_PAID_BY_PATIENT] = (amount - payment[AMOUNT_PAID_BY_INS_COMPANY]) - DISCOUNT_WITH_PLATINUM_PLAN;
        }
        return payment;
    }
}
