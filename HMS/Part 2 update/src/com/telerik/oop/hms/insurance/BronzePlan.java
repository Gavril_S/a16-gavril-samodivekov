package com.telerik.oop.hms.insurance;

public class BronzePlan extends HealthInsurancePlan {

    private static final double BRONZE_PLAN_COVERAGE = 0.6;

    public BronzePlan(){
        super(BRONZE_PLAN_COVERAGE);
    }
}
