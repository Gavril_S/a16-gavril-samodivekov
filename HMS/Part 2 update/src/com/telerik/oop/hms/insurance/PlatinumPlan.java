package com.telerik.oop.hms.insurance;

public class PlatinumPlan extends HealthInsurancePlan {

    private static final double PLATINUM_PLAN_COVERAGE = 0.9;

    public PlatinumPlan(){
        super(PLATINUM_PLAN_COVERAGE);
    }
}
