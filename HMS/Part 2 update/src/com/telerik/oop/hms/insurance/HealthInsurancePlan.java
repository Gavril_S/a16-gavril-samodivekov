package com.telerik.oop.hms.insurance;

public class HealthInsurancePlan {

    private double coverage;

    public HealthInsurancePlan(double coverage){
        setCoverage(coverage);
    }

    public double getCoverage(){
        return coverage;
    }

    private void setCoverage(double coverage){
        this.coverage = coverage;
    }
}
