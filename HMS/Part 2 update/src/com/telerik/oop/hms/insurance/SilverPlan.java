package com.telerik.oop.hms.insurance;

public class SilverPlan extends HealthInsurancePlan {

    private static final double SILVER_PLAN_COVERAGE = 0.7;

    public SilverPlan(){
        super(SILVER_PLAN_COVERAGE );
    }
}
