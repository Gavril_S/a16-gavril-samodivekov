package com.telerik.oop.hms.insurance;

public class GoldPlan extends HealthInsurancePlan {

    private static final double GOLD_PLAN_COVERAGE = 0.8;

    public GoldPlan(){
        super(GOLD_PLAN_COVERAGE);
    }
}
