package com.telerik.oop.hms.insurance;

public class NoInsurancePlan extends HealthInsurancePlan {

    private static final double NO_INSURANCE_PLAN_COVERAGE = 0.0;

    public NoInsurancePlan(){
        super(NO_INSURANCE_PLAN_COVERAGE);
    }
}
