package com.telerik.oop.hms.users;

public class Nurse extends Staff {

    public Nurse() {
    }

    public Nurse(long nurseId, String firstName, String lastName, String gender, String email, int yearsOfExperience,
                 String description, double salary, boolean insured, int age, boolean smoking) {
        super(nurseId, firstName, lastName, gender, email, yearsOfExperience, description, salary, insured, age, smoking);
    }
}
