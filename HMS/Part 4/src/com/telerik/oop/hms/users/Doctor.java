package com.telerik.oop.hms.users;

public class Doctor extends Staff {

    private String specialisation;

    public Doctor() {
    }

    public Doctor(long doctorId, String firstName, String lastName, String gender, String email, int yearsOfExperience,
                  String description, double salary, String specialisation, boolean insured, int age, boolean smoking) {
        super(doctorId, firstName, lastName, gender, email, yearsOfExperience, description, salary, insured, age, smoking);
        setSpecialisation(specialisation);
    }

    public String getSpecialisation() {
        return specialisation;
    }

    private void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }
}
