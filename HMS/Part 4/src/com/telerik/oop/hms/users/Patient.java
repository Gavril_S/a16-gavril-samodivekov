package com.telerik.oop.hms.users;

public class Patient extends User {

    public Patient() {
    }

    public Patient(long patientId, String firstName, String lastName, String gender, String email,
                   boolean insured, int age, boolean smoking) {
        super(patientId, firstName, lastName, gender, email, insured, age, smoking);

    }
}
