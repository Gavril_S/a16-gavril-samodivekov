package com.telerik.oop.hms.users;

import com.telerik.oop.hms.insurance.HealthInsurancePlan;

public class User {

    private long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private boolean insured;
    private HealthInsurancePlan insurancePlan;
    private int age;
    private boolean smoking;

    public User(){

    }

    public User(long id, String firstName, String lastName, String gender, String email,
                    boolean insured, int age, boolean smoking) {
       setId(id);
       setFirstName(firstName);
       setLastName(lastName);
       setGender(gender);
       setEmail(email);
       setInsured(insured);
       setAge(age);
       setSmoking(smoking);
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public boolean isInsured() {
        return insured;
    }

    public HealthInsurancePlan getInsurancePlan(){
        return insurancePlan;
    }

    public int getAge(){
        return age;
    }

    public boolean isSmoking(){
        return smoking;
    }

    private void setId(long id) {
        this.id = id;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private void setGender(String gender) {
        this.gender = gender;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    private void setInsured(boolean insured) {
        this.insured = insured;
    }

    public void setInsurancePlan(HealthInsurancePlan insurancePlan){
        this.insurancePlan = insurancePlan;
    }

    private void setAge(int age){
        this.age = age;
    }

    private void setSmoking(boolean smoking){
        this.smoking = smoking;
    }
}
