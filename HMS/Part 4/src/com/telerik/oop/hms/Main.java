package com.telerik.oop.hms;

import com.telerik.oop.hms.insurance.BlueCrossBlueShield;
import com.telerik.oop.hms.insurance.HealthInsurancePlan;
import com.telerik.oop.hms.insurance.InsuranceBrand;
import com.telerik.oop.hms.insurance.PlatinumPlan;
import com.telerik.oop.hms.users.User;

public class Main {

    public static void main(String[] args) {
	// write your code here
        User staff = new User();
        InsuranceBrand insuranceBrand = new BlueCrossBlueShield();
        HealthInsurancePlan insurancePlan = new PlatinumPlan();

        insurancePlan.setOfferedBy();
        staff.setInsurancePlan(insurancePlan);
        double premium = insurancePlan.computeMonthlyPremium(5000, 56, true);
        System.out.printf("%.2f", premium);
    }
}
