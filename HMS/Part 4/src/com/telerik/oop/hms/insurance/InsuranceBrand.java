package com.telerik.oop.hms.insurance;

public interface InsuranceBrand {

   double computeMonthlyPremium(HealthInsurancePlan insurancePlan, int age, boolean smoking);
}
