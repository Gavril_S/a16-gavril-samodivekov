package com.telerik.oop.hms.insurance;

public class BlueCrossBlueShield implements InsuranceBrand {

    private static final String BRAND_NAME = "Blue Cross Blue Shield";

    private long id;
    private String name;

    public BlueCrossBlueShield(){
        setName();
    }

    public BlueCrossBlueShield(long id){
        setId(id);
        setName();
    }

    @Override
    public double computeMonthlyPremium(HealthInsurancePlan insurancePlan, int age, boolean smoking) {
        double premiumAdditionForOverageAndSmoking = 0;
        if(insurancePlan instanceof PlatinumPlan){
            if(age > 55){
                premiumAdditionForOverageAndSmoking += 200;
            }
            if(smoking){
                premiumAdditionForOverageAndSmoking += 100;
            }
        } else if(insurancePlan instanceof GoldPlan){
            if(age > 55){
                premiumAdditionForOverageAndSmoking += 150;
            }
            if(smoking){
                premiumAdditionForOverageAndSmoking += 90;
            }
        } else if(insurancePlan instanceof SilverPlan){
            if(age > 55){
                premiumAdditionForOverageAndSmoking += 100;
            }
            if(smoking){
                premiumAdditionForOverageAndSmoking += 80;
            }
        } else if(insurancePlan instanceof BronzePlan){
            if(age > 55){
                premiumAdditionForOverageAndSmoking += 50;
            }
            if(smoking){
                premiumAdditionForOverageAndSmoking += 70;
            }
        }
        return premiumAdditionForOverageAndSmoking;
    }

    public long getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    private void setId(long id){
        this.id = id;
    }

    private void setName(){
        this.name = BRAND_NAME;
    }
}
