package com.telerik.oop.hms.insurance;

public class BronzePlan extends HealthInsurancePlan {

    private static final double BRONZE_PLAN_COVERAGE = 0.6;
    private static final double BRONZE_MONTHLY_PREMIUM_PERCENT = 0.05;

    public BronzePlan() {
        super(BRONZE_PLAN_COVERAGE);
    }


    public double computeMonthlyPremium(double salary, int age, boolean smoking){
        return BRONZE_MONTHLY_PREMIUM_PERCENT * salary +
                getOfferedBy().computeMonthlyPremium(this, age, smoking);
    }
}
