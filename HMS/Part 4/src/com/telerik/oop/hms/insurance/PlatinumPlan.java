package com.telerik.oop.hms.insurance;

public class PlatinumPlan extends HealthInsurancePlan {

    private static final double PLATINUM_PLAN_COVERAGE = 0.9;
    private static final double PLATINUM_MONTHLY_PREMIUM_PERCENT = 0.08;

    public PlatinumPlan(){
        super(PLATINUM_PLAN_COVERAGE);
    }


    public double computeMonthlyPremium(double salary, int age, boolean smoking){
        return PLATINUM_MONTHLY_PREMIUM_PERCENT * salary +
                getOfferedBy().computeMonthlyPremium(this, age, smoking);
    }
}
