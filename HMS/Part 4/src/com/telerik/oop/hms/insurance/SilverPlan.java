package com.telerik.oop.hms.insurance;

public class SilverPlan extends HealthInsurancePlan {

    private static final double SILVER_PLAN_COVERAGE = 0.7;
    private static final double SILVER_MONTHLY_PREMIUM_PERCENT = 0.06;

    public SilverPlan() {
        super(SILVER_PLAN_COVERAGE);
    }

    public double computeMonthlyPremium(double salary, int age, boolean smoking){
        return SILVER_MONTHLY_PREMIUM_PERCENT * salary +
                getOfferedBy().computeMonthlyPremium(this, age, smoking);
    }
}
