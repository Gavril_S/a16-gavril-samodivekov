package com.telerik.oop.hms.insurance;

public class GoldPlan extends HealthInsurancePlan {

    private static final double GOLD_PLAN_COVERAGE = 0.8;
    private static final double GOLD_MONTHLY_PREMIUM_PERCENT = 0.07;

    public GoldPlan() {
        super(GOLD_PLAN_COVERAGE);
    }


    public double computeMonthlyPremium(double salary, int age, boolean smoking){
        return GOLD_MONTHLY_PREMIUM_PERCENT * salary +
                getOfferedBy().computeMonthlyPremium(this, age, smoking);
    }
}
