package com.telerik.oop.hms.insurance;

public abstract class HealthInsurancePlan {

    private double coverage;
    private InsuranceBrand offeredBy;

    public HealthInsurancePlan(double coverage){
        setCoverage(coverage);
    }


    public abstract double computeMonthlyPremium(double salary, int age, boolean smoking);

    public double getCoverage(){
        return coverage;
    }

    public InsuranceBrand getOfferedBy(){
        return offeredBy;
    }

    private void setCoverage(double coverage){
        this.coverage = coverage;
    }

    public void setOfferedBy(){
        this.offeredBy = new BlueCrossBlueShield();
    }
}
