package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.contracts.Product;

import java.util.List;

public class VerificationHelper {
    private static final String WRONG_INPUT_LENGTH = "The length of the input doesn't meet the requirements";
    private static final String NULL_ARGUMENT_PASSED = "The value of the argument cannot be null";

    public static void parameterLengthVerification(String input, int minInputLength, int maxInputLength) {
        if (input.length() < minInputLength || input.length() > maxInputLength) {
            throw new IllegalArgumentException(WRONG_INPUT_LENGTH);
        }
    }

    public static void positiveValueCheck(double input, String wrongValue) {
        if (input < 0) {
            throw new IllegalArgumentException(wrongValue);
        }
    }

    public static void checkIfPassedArgumentIsNull(String variable){
        if(variable == null){
            throw new IllegalArgumentException(NULL_ARGUMENT_PASSED);
        }
    }

    public static void checkIfPassedArgumentIsNull(Product variable){
        if(variable == null){
            throw new IllegalArgumentException(NULL_ARGUMENT_PASSED);
        }
    }

    public static void checkIfIngredientListIsNull(List<String> ingredients){
        if(ingredients == null){
            throw new IllegalArgumentException(NULL_ARGUMENT_PASSED);
        }
    }
}
