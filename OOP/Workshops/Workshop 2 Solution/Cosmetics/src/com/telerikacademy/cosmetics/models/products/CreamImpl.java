package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductImpl implements Cream {
    private static final int MIN_INPUT_LENGTH = 3;
    private static final int MAX_INPUT_LENGTH = 15;
    private static final String WRONG_PRICE = "The price must be greater than zero";

    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        this.scent = scent;
    }

    public ScentType getScent(){
        return scent;
    }

    @Override
    public String print() {
        return String.format("#%s %s" + System.lineSeparator() +
                        "#Price: $%f" + System.lineSeparator() +
                        "#Gender: %s" + System.lineSeparator() +
                        "#Scent: %s" + System.lineSeparator(),
                getName(), getBrand(), getPrice(), getGender(), getScent());
    }

    @Override
    protected int getMinNameLength() {
        return MIN_INPUT_LENGTH;
    }

    @Override
    protected int getMinBrandLength() {
        return MIN_INPUT_LENGTH;
    }

    @Override
    protected int getMaxNameLength() {
        return MAX_INPUT_LENGTH;
    }

    @Override
    protected int getMaxBrandLength() {
        return MAX_INPUT_LENGTH;
    }

    @Override
    protected void setPrice(double price) {
        if(price <= 0){
            throw new IllegalArgumentException(WRONG_PRICE);
        }
        super.setPrice(price);
    }
}
