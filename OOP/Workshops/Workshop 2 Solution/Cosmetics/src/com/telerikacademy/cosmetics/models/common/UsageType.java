package com.telerikacademy.cosmetics.models.common;

public enum UsageType {
    EVERY_DAY,
    MEDICAL,
    EVERYDAY;


    @Override
    public String toString() {
        switch (this) {
            case MEDICAL:
                return "Medical";
            case EVERYDAY:
                return "EveryDay";
            case EVERY_DAY:
                return "EveryDay";
            default:
                throw new IllegalArgumentException();
        }
    }
}
