package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.VerificationHelper;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

public class ShampooImpl extends ProductImpl implements Shampoo {
    private static final String WRONG_MILLILITERS = "Milliliters cannot be negative";

    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        this.usage = usage;
    }


    public int getMilliliters(){
        return milliliters;
    }

    public UsageType getUsage(){
        return usage;
    }

    @Override
    public String print() {
        return String.format("%s" + System.lineSeparator() +
                            " #Milliliters: %d" + System.lineSeparator() +
                            " #Usage: %s" + System.lineSeparator(),
                            super.print(), getMilliliters(), getUsage());
    }

    private void setMilliliters(int milliliters){
        VerificationHelper.positiveValueCheck(milliliters, WRONG_MILLILITERS);
        this.milliliters = milliliters;
    }
}
