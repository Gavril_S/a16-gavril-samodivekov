package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.VerificationHelper;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public class ProductImpl implements Product {
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MIN_BRAND_LENGTH = 2;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MAX_BRAND_LENGTH = 10;
    private static final String WRONG_PRICE = "The price cannot be negative";

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender){
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName(){
        return name;
    }

    public String getBrand(){
        return brand;
    }

    public double getPrice(){
        return price;
    }

    public GenderType getGender(){
        return gender;
    }

    public String print(){
        return String.format("#%s %s" + System.lineSeparator() +
                             " #Price: $%.2f" + System.lineSeparator() +
                             " #Gender: %s",
                                getName(), getBrand(), getPrice(), getGender());
    }

    protected int getMinNameLength(){
        return MIN_NAME_LENGTH;
    }

    protected int getMinBrandLength(){
        return MIN_BRAND_LENGTH;
    }

    protected int getMaxNameLength(){
        return MAX_NAME_LENGTH;
    }

    protected int getMaxBrandLength(){
        return MAX_BRAND_LENGTH;
    }

    private void setName(String name){
        VerificationHelper.checkIfPassedArgumentIsNull(name);
        VerificationHelper.parameterLengthVerification(name, getMinNameLength(), getMaxNameLength());
        this.name = name;
    }

    private void setBrand(String brand){
        VerificationHelper.checkIfPassedArgumentIsNull(brand);
        VerificationHelper.parameterLengthVerification(brand, getMinBrandLength(), getMaxBrandLength());
        this.brand = brand;
    }

    protected void setPrice(double price){
        VerificationHelper.positiveValueCheck(price, WRONG_PRICE);
        this.price = price;
    }
}
