package com.telerikacademy.furniture.models;

public class ValidationHelper {

    public static final String ARGUMENT_CANNOT_BE_NULL = "Argument cannot be null.";
    public static final String ARGUMENT_CANNOT_BE_EMPTY = "Argument cannot be empty.";

    public static void checkIfNull(Object o){
        if(o == null){
            throw new IllegalArgumentException(ARGUMENT_CANNOT_BE_NULL);
        }
    }

    public static void checkIfEmpty(String str){
        if(str.isEmpty()){
            throw new IllegalArgumentException(ARGUMENT_CANNOT_BE_EMPTY);
        }
    }

    public static void checkIfInBounds(double value, double min, String errorMessage){
        if(value < min){
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
