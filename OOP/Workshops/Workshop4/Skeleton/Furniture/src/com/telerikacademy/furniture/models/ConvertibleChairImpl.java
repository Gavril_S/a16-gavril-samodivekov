package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.furniture.models.enums.ChairType;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class ConvertibleChairImpl extends ChairImpl implements ConvertibleChair {

    private double height;
    private final double initialHeight;
    private String status;

    public ConvertibleChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height, numberOfLegs);
        initialHeight = height;
    }

    @Override
    public ChairType getChairType() {
        return ChairType.CONVERTIBLE;
    }

    @Override
    public boolean getConverted() {
        if(height == 0.10){
            return true;
        }
        return false;
    }

    @Override
    public void convert() {
        if(!getConverted()){
            height = 0.10;
            super.setHeight(height);
        } else {
            height = initialHeight;
            super.setHeight(height);
        }
    }

    @Override
    public String additionalInfo() {
        return String.format("Legs: %d, State: %s", getNumberOfLegs(), getStatus());
    }

    private String getStatus(){
        if(getConverted()){
            status = "Converted";
        } else {
            status = "Normal";
        }
        return status;
    }
}
