package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Chair;
import com.telerikacademy.furniture.models.enums.ChairType;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class ChairImpl extends FurnitureImpl implements Chair {
    private static final int MIN_NUMBER_OF_LEGS = 0;
    private static final String NUMBER_OF_LEGS_ERROR = "Number of legs cannot be a negative number.";
    private int numberOfLegs;

    public ChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height);
        setNumberOfLegs(numberOfLegs);
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public ChairType getChairType(){
        return ChairType.NORMAL;
    }

    @Override
    public String additionalInfo() {
        return String.format("Legs: %d", getNumberOfLegs());
    }

    private void setNumberOfLegs(int numberOfLegs){
        ValidationHelper.checkIfInBounds(numberOfLegs, MIN_NUMBER_OF_LEGS, NUMBER_OF_LEGS_ERROR);
        this.numberOfLegs = numberOfLegs;
    }

}
