package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Furniture;
import com.telerikacademy.furniture.models.enums.MaterialType;

public abstract class FurnitureImpl implements Furniture {
    private static final int MIN_MODEL_LENGTH = 3;
    private static final String MODEL_LENGTH_ERROR = "Model cannot be less than 3 symbols.";
    private static final double MIN_PRICE_VALUE = 0.01;
    private static final String PRICE_ERROR = "Price must be greater than 0.";
    private static final double MIN_HEIGHT_VALUE = 0.01;
    private static final String HEIGHT_ERROR = "Height must be greater than 0.";
    private String model;
    private MaterialType materialType;
    private double price;
    private double height;

    public FurnitureImpl(String model, MaterialType materialType, double price, double height) {
        setModel(model);
        setMaterialType(materialType);
        setPrice(price);
        setHeight(height);
    }

    public String toString(){
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f, %s",
                this.getClass().getSimpleName().replace("Impl", ""), getModel(),
                getMaterialType(), getPrice(), getHeight(), additionalInfo());
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public MaterialType getMaterialType() {
        return materialType;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public double getHeight() {
        return height;
    }

    public abstract String additionalInfo();

    private void setModel(String model) {
        ValidationHelper.checkIfNull(model);
        ValidationHelper.checkIfEmpty(model);
        ValidationHelper.checkIfInBounds(model.length(), MIN_MODEL_LENGTH, MODEL_LENGTH_ERROR);
        this.model = model;
    }

    private void setMaterialType(MaterialType materialType) {
        ValidationHelper.checkIfNull(materialType);
        this.materialType = materialType;
    }

    private void setPrice(double price) {
        ValidationHelper.checkIfInBounds(price, MIN_PRICE_VALUE, PRICE_ERROR);
        this.price = price;
    }

    protected void setHeight(double height) {
        ValidationHelper.checkIfInBounds(height, MIN_HEIGHT_VALUE, HEIGHT_ERROR);
        this.height = height;
    }
}
