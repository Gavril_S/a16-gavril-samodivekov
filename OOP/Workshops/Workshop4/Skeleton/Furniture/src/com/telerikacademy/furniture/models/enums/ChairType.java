package com.telerikacademy.furniture.models.enums;

public enum ChairType {
    NORMAL,
    ADJUSTABLE,
    CONVERTIBLE
}
