package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Company;
import com.telerikacademy.furniture.models.contracts.Furniture;

import java.util.ArrayList;
import java.util.List;

public class CompanyImpl implements Company {
    private static final int MIN_NAME_LENGTH = 5;
    private static final String NAME_ERROR = "Company name cannot be less than 5 symbols long.";
    private static final int REGISTRATION_NUMBER_LENGTH = 10;
    private static final String REGISTRATION_NUMBER_LENGTH_ERROR = "The registration number must be exactly 10 symbols long.";
    private static final String REGISTRATION_NUMBER_DIGITS_ERROR = "The registration number must contain only digits.";

    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;

    public CompanyImpl(String name, String registrationNumber) {
        setName(name);
        setRegistrationNumber(registrationNumber);
        this.furnitures = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public List<Furniture> getFurnitures() {
        return new ArrayList<>(furnitures);
    }

    public void add(Furniture furniture) {
        ValidationHelper.checkIfNull(furniture);
        ValidationHelper.checkIfEmpty(furniture.toString());
        furnitures.add(furniture);
    }

    public String catalog() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("%s - %s - %s %s",
                    getName(), getRegistrationNumber(),
                    furnitures.isEmpty() ? "no" : String.format("%d", furnitures.size()),
                    furnitures.size() == 1 ? "furniture" : "furnitures"));

        furnitures.sort((x1, x2) -> {
                        int compareResult = (int) (x1.getPrice() - x2.getPrice());
                        if(compareResult == 0){
                            compareResult = x1.getModel().compareTo(x2.getModel());
                        }
                        return compareResult;
        });

        if(!furnitures.isEmpty()){
            for (Furniture furn : furnitures) {
                strBuilder.append(System.lineSeparator());
                strBuilder.append(furn);
            }
        }

        return strBuilder.toString().trim();
    }

    public Furniture find(String model) {
        ValidationHelper.checkIfNull(model);
        ValidationHelper.checkIfEmpty(model);
        for (Furniture furniture : furnitures) {
            if(furniture.getModel().equals(model)){
                return furniture;
            }
        }
        return null;
    }

    public void remove(Furniture furniture) {
        ValidationHelper.checkIfNull(furniture);
        ValidationHelper.checkIfEmpty(furniture.toString());
        for (Furniture furn : furnitures) {
            if(furn.equals(furniture)){
                furnitures.remove(furn);
                break;
            }
        }
    }

    private void setName(String name){
        ValidationHelper.checkIfNull(name);
        ValidationHelper.checkIfEmpty(name);
        ValidationHelper.checkIfInBounds(name.length(), MIN_NAME_LENGTH, NAME_ERROR);
        this.name = name;
    }

    private void setRegistrationNumber(String registrationNumber){
        if(registrationNumber.length() != REGISTRATION_NUMBER_LENGTH){
            throw new IllegalArgumentException(REGISTRATION_NUMBER_LENGTH_ERROR);
        }
        if(!registrationNumber.matches("[0-9]+")){
            throw new IllegalArgumentException(REGISTRATION_NUMBER_DIGITS_ERROR);
        }
        this.registrationNumber = registrationNumber;
    }
}
