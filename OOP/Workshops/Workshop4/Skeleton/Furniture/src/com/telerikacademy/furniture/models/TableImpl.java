package com.telerikacademy.furniture.models;

import com.telerikacademy.furniture.models.contracts.Table;
import com.telerikacademy.furniture.models.enums.MaterialType;

public class TableImpl extends FurnitureImpl implements Table {
    private static final double MIN_LENGTH_VALUE = 0.01;
    private static final String LENGTH_ERROR = "Length must be greater than 0.";
    private static final double MIN_WIDTH_VALUE = 0.01;
    private static final String WIDTH_ERROR = "Width must be greater than 0.";
    private double length;
    private double width;

    public TableImpl(String model, MaterialType materialType, double price, double height, double length, double width) {
        super(model, materialType, price, height);
        setLength(length);
        setWidth(width);
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return getLength() * getWidth();
    }

    @Override
    public String additionalInfo() {
        return String.format("Length: %.2f, Width: %.2f, Area: %.4f", getLength(), getWidth(), getArea());
    }

    private void setLength(double length) {
        ValidationHelper.checkIfInBounds(length, MIN_LENGTH_VALUE, LENGTH_ERROR);
        this.length = length;
    }

    private void setWidth(double width) {
        ValidationHelper.checkIfInBounds(width, MIN_WIDTH_VALUE, WIDTH_ERROR);
        this.width = width;
    }
}
