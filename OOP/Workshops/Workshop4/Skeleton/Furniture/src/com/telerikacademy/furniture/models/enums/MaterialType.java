package com.telerikacademy.furniture.models.enums;

public enum MaterialType {
    WOODEN,
    LEATHER,
    PLASTIC;


    @Override
    public String toString() {
        switch (this){
            case LEATHER:
                return "Leather";
            case WOODEN:
                return "Wooden";
            case PLASTIC:
                return "Plastic";
                default:
                    throw new IllegalArgumentException();
        }
    }
}
