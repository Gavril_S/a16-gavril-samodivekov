package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.products.Product;


import java.util.*;

public class Category {
    private static final int NAME_MIN_VALUE = 2;
    private static final int NAME_MAX_VALUE = 15;
    private static final String NAME_LENGTH_MISMATCH = "Wrong name input length";
    private static final String PRODUCT_NOT_FOUND = "There is no product in the category";

    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    public void setName(String name) {
        if(name.length() < NAME_MIN_VALUE || name.length() > NAME_MAX_VALUE){
            throw new IllegalArgumentException(NAME_LENGTH_MISMATCH);
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Product> getProducts() {
       return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        Validation.checkIfNull(product);
        products.add(product);
    }

    public void removeProduct(Product product) {
        Validation.checkIfNull(product);
        if(!products.contains(product)) {
            throw new IllegalArgumentException(PRODUCT_NOT_FOUND);
        } else {
            products.remove(product);
        }
    }

    public String print() {
        StringBuilder strbld = new StringBuilder(String.format("Category: " + getName() + System.lineSeparator()));
        if(products.isEmpty()){
            strbld.append(" #" + PRODUCT_NOT_FOUND);
        } else {
            for (Product prod : products) {
                strbld.append(prod.print());
            }
        }

        return strbld.toString();
    }
}
