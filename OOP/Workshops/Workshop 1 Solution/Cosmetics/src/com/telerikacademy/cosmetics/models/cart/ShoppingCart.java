package com.telerikacademy.cosmetics.models.cart;

import com.telerikacademy.cosmetics.models.Validation;
import com.telerikacademy.cosmetics.models.products.Product;


import java.util.*;

public class ShoppingCart {

    private List<Product> productList;

    public ShoppingCart() {
        productList = new ArrayList<>();
    }

    public List<Product> getProductList() {
        return new ArrayList<>(productList);
    }

    public void addProduct(Product product) {
        Validation.checkIfNull(product);
        productList.add(product);
    }

    public void removeProduct(Product product) {
        Validation.checkIfNull(product);
        productList.remove(product);
    }


    public boolean containsProduct(Product product) {
        Validation.checkIfNull(product);
        if(productList.contains(product)){
            return true;
        } else {
            return false;
        }
    }

    public double totalPrice() {
        double sum = 0;
        for (Product prod : productList) {
            sum += prod.getPrice();
        }
        return sum;
    }
}
