package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.products.Product;

public class Validation {

    private static final String NULL_POINTER_MISMATCH = "Invalid product.";

    public static void checkIfNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(NULL_POINTER_MISMATCH);
        }
    }
}
