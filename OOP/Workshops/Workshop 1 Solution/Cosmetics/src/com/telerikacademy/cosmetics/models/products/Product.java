package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;

public class Product {
    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 10;
    private static final int BRAND_MIN_LENGTH = 2;
    private static final int BRAND_MAX_LENGTH = 10;
    private static final String NAME_LENGTH_MISMATCH = "Wrong name input length.";
    private static final String BRAND_LENGTH_MISMATCH = "Wrong brand input length";
    private static final String PRICE_VALUE_MISMATCH = "Price must be greater or equal to 0";


    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public GenderType getGender() {
        return gender;
    }


    private void setName(String name) {
        if(name.length() >= NAME_MIN_LENGTH  && name.length() <= NAME_MAX_LENGTH){
            this.name = name;
        } else {
            throw new IllegalArgumentException(NAME_LENGTH_MISMATCH);
        }
    }

    private void setBrand(String brand) {
        if(brand.length() >= BRAND_MIN_LENGTH && brand.length() <= BRAND_MAX_LENGTH){
            this.brand = brand;
        } else {
            throw new IllegalArgumentException(BRAND_LENGTH_MISMATCH);
        }
    }

    private void setPrice(double price) {
        if(price >= 0){
            this.price = price;
        } else {
            throw new IllegalArgumentException(PRICE_VALUE_MISMATCH);
        }
    }

    public String print() {
        return String.format("#" + getName() + " " + getBrand() + System.lineSeparator() +
                            "#Price: " + getPrice() + System.lineSeparator() +
                            "#Gender: " + getGender() + System.lineSeparator() +
                            "===" + System.lineSeparator());
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
    }
}
