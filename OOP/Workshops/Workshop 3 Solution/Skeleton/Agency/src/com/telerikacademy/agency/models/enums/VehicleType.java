package com.telerikacademy.agency.models.enums;

public enum VehicleType {
    LAND,
    AIR,
    SEA
}
