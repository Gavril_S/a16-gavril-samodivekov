package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.enums.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleImpl implements Vehicle {

    private static final int MIN_PASSENGER_CAPACITY = 1;
    private static final int MAX_PASSENGER_CAPACITY = 800;
    private static final double MIN_PRICE_PER_KILOMETER = 0.10;
    private static final double MAX_PRICE_PER_KILOMETER = 2.50;
    private static final String ILLEGAL_VEHICLE_PASSENGERS= "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private static final String ILLEGAL_VEHICLE_PRICE_PER_KILOMETER = "A vehicle with a price per kilometer lower than $0.10 or higher than " +
            "$2.50 cannot exist!";

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;

    public VehicleImpl() {

    }

    public VehicleImpl(int passengerCapacity, double pricePerKilometer, VehicleType type){
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(type);
    }

    public int getPassengerCapacity(){
        return passengerCapacity;
    }

    public double getPricePerKilometer(){
        return pricePerKilometer;
    }

    public VehicleType getType(){
        return type;
    }

    @Override
    public String toString() {
        return String.format("Passenger capacity: %d" + System.lineSeparator() +
                        "Price per kilometer: %.2f" + System.lineSeparator() +
                        "Vehicle type: %s",
                getPassengerCapacity(), getPricePerKilometer(), getType());
    }

    protected int minPassengerCapacity(){
        return MIN_PASSENGER_CAPACITY;
    }

    protected int maxPassengerCapacity(){
        return MAX_PASSENGER_CAPACITY;
    }

    protected String illegalVehicleMessage(){
        return ILLEGAL_VEHICLE_PASSENGERS;
    }


    private void setPassengerCapacity(int passengerCapacity){
        ValidationHelper.checkIfValueIsInBounds(passengerCapacity, minPassengerCapacity(), maxPassengerCapacity(), illegalVehicleMessage());
        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer){
        if(pricePerKilometer < MIN_PRICE_PER_KILOMETER || pricePerKilometer > MAX_PRICE_PER_KILOMETER){
            throw new IllegalArgumentException(ILLEGAL_VEHICLE_PRICE_PER_KILOMETER);
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    private void setType(VehicleType type){
        ValidationHelper.checkIfNull(type);
        this.type = type;
    }
}
