package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.enums.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleImpl implements Train {

    private static final int MIN_CARTS_VALUE = 1;
    private static final int MAX_CARTS_VALUE = 15;
    private static final int MIN_PASSENGER_CAPACITY = 30;
    private static final int MAX_PASSENGER_CAPACITY = 150;
    private static final String TRAIN_CART_LIMITS = "A train cannot have less than 1 or more than 15 carts.";
    private static final String TRAIN_PASSENGER_LIMITS = "A train cannot have less than 30 passengers or more than 150 passengers.";

    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
    }

    public int getCarts(){
        return carts;
    }

    @Override
    public String toString() {
        return String.format("Train ----" + System.lineSeparator() +
                        "%s" + System.lineSeparator() +
                        "Carts amount: %d" + System.lineSeparator(),
                super.toString(), getCarts());
    }

    private void setCarts(int carts){
        ValidationHelper.checkIfValueIsInBounds(carts, MIN_CARTS_VALUE, MAX_CARTS_VALUE, TRAIN_CART_LIMITS);
        this.carts = carts;
    }

    @Override
    protected int minPassengerCapacity() {
        return MIN_PASSENGER_CAPACITY;
    }

    @Override
    protected int maxPassengerCapacity() {
        return MAX_PASSENGER_CAPACITY;
    }

    @Override
    protected String illegalVehicleMessage() {
        return TRAIN_PASSENGER_LIMITS;
    }
}
