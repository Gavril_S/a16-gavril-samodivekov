package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.enums.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleImpl implements Airplane {

    private boolean freeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean freeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        setFreeFood(freeFood);
    }

    public boolean hasFreeFood(){
        return freeFood;
    }

    @Override
    public String toString() {
        return String.format("Airplane ----" + System.lineSeparator() +
                        "%s" + System.lineSeparator() +
                        "Has free food: %b" + System.lineSeparator(),
                         super.toString(), hasFreeFood());
    }

    private void setFreeFood(boolean freeFood){
        this.freeFood = freeFood;
    }
}
