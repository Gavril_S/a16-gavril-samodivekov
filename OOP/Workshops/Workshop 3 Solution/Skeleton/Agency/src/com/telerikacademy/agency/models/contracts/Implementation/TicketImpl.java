package com.telerikacademy.agency.models.contracts.Implementation;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private Journey journey;
    private double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        setJourney(journey);
        setAdministrativeCosts(administrativeCosts);
    }

    public double calculatePrice(){
        return getAdministrativeCosts() * journey.calculateTravelCosts();
    }

    public String toString(){
        return String.format("Ticket ----" + System.lineSeparator() +
                            "Destination: %s" + System.lineSeparator() +
                            "Price: %.2f" + System.lineSeparator(),
                            getJourney().getDestination(), calculatePrice());
    }

    public Journey getJourney(){
        return journey;
    }

    public double getAdministrativeCosts(){
        return administrativeCosts;
    }

    private void setJourney(Journey journey){
        ValidationHelper.checkIfNull(journey);
        this.journey = journey;
    }

    private void setAdministrativeCosts(double administrativeCosts){
        this.administrativeCosts = administrativeCosts;
    }
}
