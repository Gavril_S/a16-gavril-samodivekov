package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.enums.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleImpl implements Bus {

    private static final int MIN_PASSENGER_CAPACITY = 10;
    private static final int MAX_PASSENGER_CAPACITY = 50;
    private static final String ILLEGAL_NUMBER_OF_PASSENGERS = "A bus cannot have less than 10 passengers or more than 50 passengers.";

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
    }

    @Override
    public String toString() {
        return String.format("Bus ----" + System.lineSeparator() +
                "%s" + System.lineSeparator(), super.toString());
    }

    @Override
    protected int minPassengerCapacity() {
        return MIN_PASSENGER_CAPACITY;
    }

    @Override
    protected int maxPassengerCapacity() {
        return MAX_PASSENGER_CAPACITY;
    }

    @Override
    protected String illegalVehicleMessage() {
        return ILLEGAL_NUMBER_OF_PASSENGERS;
    }
}
