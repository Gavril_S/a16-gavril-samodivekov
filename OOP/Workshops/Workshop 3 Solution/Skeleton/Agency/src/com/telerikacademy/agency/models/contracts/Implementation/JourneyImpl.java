package com.telerikacademy.agency.models.contracts.Implementation;

import com.telerikacademy.agency.models.ValidationHelper;
import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class JourneyImpl implements Journey {

    private static final int START_LOCATION_AND_DESTINATION_MIN_LENGTH = 5;
    private static final int START_LOCATION_AND_DESTINATION_MAX_LENGTH = 25;
    private static final int DISTANCE_MIN_VALUE = 5;
    private static final int DISTANCE_MAX_VALUE = 5000;
    private static final String ILLEGAL_STARTING_LOCATION = "The StartingLocation's length cannot be less than 5 or more than 25 symbols long.";
    private static final String ILLEGAL_DESTINATION = "The Destination's length cannot be less than 5 or more than 25 symbols long.";
    private static final String ILLEGAL_DISTANCE = "The Distance cannot be less than 5 or more than 5000 kilometers.";

    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;


    public JourneyImpl() {

    }

    public JourneyImpl(String startLocation, String destination, int distance, Vehicle vehicle) {
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        setVehicle(vehicle);
    }

    public String getStartLocation(){
        return startLocation;
    }

    public String getDestination(){
        return destination;
    }

    public int getDistance(){
        return distance;
    }

    public Vehicle getVehicle(){
        return vehicle;
    }


    public double calculateTravelCosts(){
        return getDistance() * vehicle.getPricePerKilometer();
    }

    public String toString(){
        return String.format("Journey ----" + System.lineSeparator() +
                            "Start location: %s" + System.lineSeparator() +
                            "Destination: %s" + System.lineSeparator() +
                            "Distance: %d" + System.lineSeparator() +
                            "Vehicle type: %s" + System.lineSeparator() +
                            "Travel costs: %.2f" + System.lineSeparator(),
                            getStartLocation(), getDestination(), getDistance(), getVehicle().getType(), calculateTravelCosts());
    }

    private void setStartLocation(String startLocation){
        ValidationHelper.checkIfNull(startLocation);
        if(startLocation.length() < START_LOCATION_AND_DESTINATION_MIN_LENGTH || startLocation.length() > START_LOCATION_AND_DESTINATION_MAX_LENGTH){
            throw new IllegalArgumentException(ILLEGAL_STARTING_LOCATION);
        }
        this.startLocation = startLocation;
    }

    private void setDestination(String destination){
        ValidationHelper.checkIfNull(destination);
        if(destination.length() < START_LOCATION_AND_DESTINATION_MIN_LENGTH || destination.length() > START_LOCATION_AND_DESTINATION_MAX_LENGTH){
            throw new IllegalArgumentException(ILLEGAL_DESTINATION);
        }
        this.destination = destination;
    }

    private void setDistance(int distance){
        if(distance < DISTANCE_MIN_VALUE || distance > DISTANCE_MAX_VALUE){
            throw new IllegalArgumentException(ILLEGAL_DISTANCE);
        }
        this.distance = distance;
    }

    private void setVehicle(Vehicle vehicle){
        ValidationHelper.checkIfNull(vehicle);
        this.vehicle = vehicle;
    }
}
