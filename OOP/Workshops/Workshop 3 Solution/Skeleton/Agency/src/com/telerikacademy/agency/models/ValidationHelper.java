package com.telerikacademy.agency.models;

public class ValidationHelper {

    private static final String NULL_ARGUMENT = "An argument cannot be null.";

    public static void checkIfNull(Object value) {
        if (value == null) {
            throw new IllegalArgumentException(NULL_ARGUMENT);
        }
    }

    public static void checkIfValueIsInBounds(int value, int min, int max, String message) {
        if(value < min || value > max){
            throw new IllegalArgumentException(message);
        }
    }
}
